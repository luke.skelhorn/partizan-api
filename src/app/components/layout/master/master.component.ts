import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {

  roleId: string;
  chart = [];

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) {
    this.roleId = this._authService.getRoleID();
  }

  ngOnInit() {
  }

  signOut() {
    this._authService.signOut();
  }

}
