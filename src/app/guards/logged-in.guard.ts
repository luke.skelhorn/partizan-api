import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TokenService } from '../services/token.service';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(
    private _tokenService: TokenService,
    private _router: Router
  ) {}

 canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._tokenService.isAuthenticated()) {
      return true;
    } else {
      this._router.navigate(['/signin']);
      return false;
    }
  }
}
