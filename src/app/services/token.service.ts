import { Injectable } from '@angular/core';
import { Token } from '../models/token';

@Injectable()
export class TokenService {

  private token: Token;

  constructor() { }

  isAuthenticated() {
    if (this.getAccessToken()) {
        return true;
    }
    return false;
  }

  getAccessToken() {
        if (localStorage.getItem('oauth_access_token') === null) {
        return false;
    }
    return localStorage.getItem('oauth_access_token');
  }

  setAccessToken(token) {
    localStorage.setItem('oauth_access_token', JSON.stringify(token));
  }

  deleteAccessToken() {
    localStorage.removeItem('oauth_access_token');
  }

  setRoleId(token) {
    localStorage.setItem('role_id', token);
  }

  deleteRoleId() {
    localStorage.removeItem('role_id');
  }

}
